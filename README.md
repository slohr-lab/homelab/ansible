# ansible

ansible playbooks to automate server configuration and tasks

# roles

## apt
update and upgrades target basically ``apt update && apt upgrade -y``

## deploy-ssh-user
accepts multiple users (name + public key) as input as well as users that are gonna be deleted. 
````yaml
  vars:
    users:
      - name: slohr
        key: "{{ lookup('file', 'slohr.pub') }}"
      - name: ansible
        key:  "{{ lookup('file', 'ansible.pub') }}"
    remove_users:
      - stefan
````
the role will create users, store the public certificate for the user as well add the user to newly created admin group (which has sudo access).

## quemu-guest-agent
installs qemu-quest-agent. this is needed for all my vms because i use proxmox as a hypervisior.

## zsh
since i use zsh as my default shell this role installs and configures zsh to my liking. a list of users needs to be provided.
````yaml
  vars:
    users:
      - slohr
      - ansible
````
